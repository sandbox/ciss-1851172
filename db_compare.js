Drupal.behaviors.dbCompare = function(context) {

    $('table').click(function(e) {
        var el = $(e.target);
        if(el.is('td')) {
            el.toggleClass('fixed');
            // Required for table.sticky-header
            $(window).trigger('resize');
        }
    });
    $('h4.collapsible').click(function() {
        $(this).next('div.collapsible').andSelf().toggleClass('collapsed');
    });

};
